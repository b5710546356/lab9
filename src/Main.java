import java.io.IOException;
import java.net.URL;
/**
 * Main class is Application class to run WordCounter for count words and syllables of URL
 * @author Phasin Sarunpornkul
 *
 */

public class Main {
	/**
	 * to run WordCounter and print elapsed time, number of words and syllables on console
	 * @param args not used
	 */
	public static void main(String[] args) {
		final String DICT_URL = "http://se.cpe.ku.ac.th/dictionary.txt";
		try{
		URL url = new URL( DICT_URL );
		WordCounter counter = new WordCounter();
		StopWatch timer = new StopWatch();
		timer.start();
		int wordcount = counter.countWords( url );
		int syllables = counter.getSyllableCount();
		timer.stop();
		System.out.printf("Reading words from %s\n",DICT_URL);
		System.out.printf("Counted %,d syllables in %,d words\n",syllables,wordcount);
		System.out.printf("Elapsed time: %.3f sec",timer.getElapsed());
		}
		catch(IOException e ){
			System.out.println(e);
		}
	}
}
