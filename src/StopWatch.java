

/**
 * A StopWatch that measures the elapsed time 
 * between starting time and stopping time or until present time.
 * @author Phasin Sarunpornkul
 *
 */
public class StopWatch {
	/** constant for converting nanoseconds to seconds. */
	private static final double NANOSECONDS = 1.0E-9;
	/** Starting time of stopwatch with nanoseconds unit. */
	private long startTime;
	/** Present time of stopwatch while it running or stopped with nanoseconds unit. */
	private long currenttime;
	/** boolean that check the stopwatch is stopped or not?*/
	private boolean isStopped;

	/** 
     * Constructor for a new stopwatch. 
     */
	public StopWatch(){
		this.startTime = 0;
		this.currenttime = 0;
		this.isStopped = true;
	}

	/**
	 * if this stopwatch isn't running,reset current time 
	 * and measure new starting time and set it is running.
	 */
	public void start(){
		if(isRunning()==false){
			this.startTime = System.nanoTime();
			this.currenttime = 0;
			this.isStopped = false;
		}
	}
	
	/**
	 * if this stopwatch is running, set it stopped 
	 * and measure stopping time.
	 */
	public void stop(){
		if(this.isStopped==false){
			this.currenttime = System.nanoTime();
			this.isStopped = true;
		}
	}

	/**
	 * to check this stopwatch is running or not?
	 * @return be true if this stopwatch is running (not stopped).
	 */
	public boolean isRunning(){
		if(this.isStopped==false){
			return true;
		}
		return false;
	}

	/**
	 * to get the elapsed time between starting time 
	 * and stopping time or until present time.
	 * @return elapsed time with second unit.
	 */
	public double getElapsed(){
		if(isRunning()==true){
			return (double)((System.nanoTime()-this.startTime)*NANOSECONDS);
		}
		else{
			return(double)(( this.currenttime-this.startTime)*NANOSECONDS);
		}
	}
}
