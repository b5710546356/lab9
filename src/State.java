/**
 * State is interface that contain handleChar and enterState method
 * @author Phasin Sarunpornkul
 *
 */
public interface State {
	/**
	 * to check the state of character  
	 * @param c is character
	 */
	public void handleChar(char c);
	/**
	 * to do some work then enter each state
	 */
	public void enterState();
}
