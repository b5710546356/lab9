import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
/**
 * WordCounter class for count syllables of word, and word
 * @author Phasin Sarunpornkul
 *
 */

public class WordCounter  {
	/*syllables to store counting syllables of word*/
	private int syllables=0, totalSyllables=0;
	/*state for check the status of WordCounter*/
	private State state;
	
	/**
	 * State class
	 */
	State START = new State(){
		public void handleChar(char c){
			if (c=='e'||c=='E') setState(E_FIRST);
			else if(isVowel(c))setState(VOWEL);
			else if (isLetter(c)) setState(CONSONANT);
			else if (c=='-'||c=='\'') setState(DASH) ;
			else setState(NOWORDSTATE);
		}
		public void enterState(){}
	};

	State VOWEL = new State(){
		public void handleChar(char c){
			if(isVowel(c))setState(VOWEL);
			else if (isLetter(c)) {
				setState(CONSONANT);
				enterState();
			}
			else if (c=='-'||c=='\''){
				setState(DASH) ;
				syllables++;
			}
			else setState(NOWORDSTATE);
		}
		public void enterState(){
			syllables++;
		}
	};

	State CONSONANT = new State(){
		public void handleChar(char c){
			if (c=='e'||c=='E') setState(E_FIRST);
			else if(isVowel(c)||c=='y'||c=='Y')setState(VOWEL);
			else if (isLetter(c)) ;
			else if (c=='-'||c=='\'')setState(DASH) ;
			else setState(NOWORDSTATE);
		}
		public void enterState(){
			syllables++;
		}
	};

	State E_FIRST = new State(){
		public void handleChar(char c){
			if(isVowel(c))setState(VOWEL);
			else if (isLetter(c)) {
				setState(CONSONANT);
				enterState();
			}
			else if (c=='-'||c=='\'')setState(DASH) ;
			else setState(NOWORDSTATE);
		}
		public void enterState(){
			syllables++;
		}
	};

	State DASH = new State(){
		public void handleChar(char c){
			if(isVowel(c))setState(VOWEL);
			else if (isLetter(c)) setState(CONSONANT);
			else if (c=='-'||c=='\'') ;
			else setState(NOWORDSTATE);
		}
		public void enterState(){
			syllables++;
		}
	};

	State NOWORDSTATE = new State(){
		public void handleChar(char c){
		}
		public void enterState(){
			syllables++;
		}
	};

	/**
	 * check that character is vowel or not
	 * @param c is character that want to check
	 * @return true if character is vowel
	 */
	public boolean isVowel(char c){
		if("AEIOUaeiou".indexOf(c)>=0) return true;
		return false;
	}
	
	/**
	 * check that character is letter or not
	 * @param c is character that want to check
	 * @return true if character is letter
	 */
	public boolean isLetter(char c){
		if(Character.isLetter(c)) return true;
		return false;
	}

	/**
	 * setting the state 
	 * @param newstate is state that to change
	 */
	public void setState(State newstate){
		this.state = newstate;
	}

	/**
	 * to counting syllables of each word
	 * @param word that want to count the syllables
	 * @return number of syllables of this word
	 */
	public int countSyllables(String word){
		syllables =0 ;
		state=START;
		for(int i=0;i<word.length();i++){
			char c = word.charAt(i);
			state.handleChar(c);
			if(state==NOWORDSTATE){
				syllables =0;
				break;
			}
			if(state==E_FIRST&&i==word.length()-1){
				if(syllables==0)syllables++;
			}
			if(state==DASH){
				if(i==0||i==word.length()-1){
					syllables =0;
					break;
				}
			}
			if(state==VOWEL&&i==word.length()-1){
				syllables++;
			}
		}
		return syllables;
	}

	/**
	 * to count the number of words in input stream
	 * @param instream is input stream that want to count
	 * @return number of word  in this input stream
	 */
	public int countWords(InputStream instream) {
		BufferedReader breader = new BufferedReader( new InputStreamReader(instream) );
		int count=0;
		try{
			while( true ) {
				String line = breader.readLine( );
				if (line == null) break;
				count++;
				totalSyllables += this.countSyllables(line);
			}
			return count;}
		catch(IOException e){
			return -1;
		}
	}
	
	/**
	 * to count the number of words in url
	 * @param url is link of data in website
	 * @return the number of words in this url
	 */
	public int countWords(URL url) {
		try{
			InputStream in = url.openStream();
			return this.countWords(in);
		}
		catch(IOException e){
			return -1;
		}
	}

	/**
	 * to get the total number of syllable in WordCounter 
	 * @return the total number of syllable
	 */
	public int getSyllableCount(){
		return this.totalSyllables;
	}
}

